<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'img' => 'img/popular5.png',
        'details' => $faker->paragraph(5),
        'price' => 100
    ];
});
