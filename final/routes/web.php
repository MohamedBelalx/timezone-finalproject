<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "IndexController@index")->name('index');

Route::get('/product/details/{id}',"IndexController@show")->name('pddetails');

Route::get('/shop',"IndexController@all")->name('shop');

Route::get('/about',function()
{
    return view('about');
})->name('about');


Route::get('/contact',function()
{
    return view('contact');
})->name('contact');

Route::get('/cart',"CartController@index")->name('cart');
//pddetails

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/product',"ProductController@index")->name('all');
Route::get('/product/create','ProductController@create')->name('create');
Route::post('/product/store','ProductController@store')->name('store');
Route::get('/product/destroy/{id}','ProductController@destroy')->name('destroy');
Route::get('/product/trashed','ProductController@trashed')->name('trashed');
Route::get('/product/restore/{id}','ProductController@restore')->name('restore');
Route::get('/product/forceDelete/{id}','ProductController@forceDelete')->name('forceDelete');
Route::get('/product/edit/{id}','ProductController@edit')->name('edit');
Route::post('/product/update/{id}','ProductController@update')->name('update');

Route::post('/cart/add/{id}','CartController@add')->name('cart.add');
Route::get('/cart/clear','CartController@clear')->name('cart.clear');
Route::get('/cart/remove/{id}','CartController@remove')->name('cart.remove');


Route::post('/checkout',"CartController@pay")->name('checkout');