<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = DB::table('product')->orderBy('id','DESC')->paginate(15);

        return view('admin.show')->with('product',$product);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $img = $request->img;

        $image_new_name = time().$img->getClientOriginalName();

        $img->move('img',$image_new_name);

        $product = Product::create([
            'name' => $request->name,
            'details' => $request->details,
            'img' => 'img/'. $image_new_name,
            'price' => $request->price
        ]);


        return redirect()->route('all');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);

        return view('admin.edit')->with('product',$product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);

        $product->name = $request->name;
        $product->details = $request->details;
        $product->price = $request->price;

        $product->save();

        return redirect()->route('all');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);

        $product->delete();

        return redirect()->back();
    }

    public function trashed()
    {
        $product = Product::onlyTrashed()->get();
        return view('admin.trashed')->with('product',$product);
    }
    public function restore($id)
    {
        $product = Product::withTrashed()->where('id',$id)->first();
        $product->restore();
        return redirect()->back();
    }

    public function forceDelete($id)
    {
        $product = Product::withTrashed()->where('id',$id)->first();
        $product->forceDelete();
        return redirect()->back();
    }
}
