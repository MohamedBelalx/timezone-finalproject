<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Cart;
use Stripe\Stripe;
use Stripe\Charge;
use Mail;
class CartController extends Controller
{

    public function index()
    {
        return view('cart');
    }
    public function add(Request $request,$id)
    {
        $product = Product::findOrFail($id);
        Cart::add([
            'id' => $request->id,
            'name' => $product->name,
            'price' => $product->price,
            'qty' => $request->qty,
            'weight' => 50,
            'options' => ['img' => $product->img]
        ]);
        return redirect()->route('cart');
    }
    public function clear()
    {
        Cart::destroy();

        return redirect()->back();
    }

    public function remove($id)
    {
        Cart::remove($id);

        return redirect()->back();
    }
    public function pay(Request $request)
    {
        Stripe::setApiKey('sk_test_LjOUxYxRY0dGwx6GgF3on61R008uWX4wOe');
        Charge::create([
            'amount' => Cart::subtotal() * 100,
            'currency' => 'usd',
            'receipt_email' => $request->email,
            'source' => $request->stripeToken
        ]);


        $to = $request->mail;

        $data = ['name' => $to,'content' => Cart::Content()];
        Mail::send('mail',$data,function($message) use($to)
        {
            $message->to($to)->subject('Order is complete');
        });

        Cart::destroy();
    }
}
