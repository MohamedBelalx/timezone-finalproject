<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    public function index()
    {
        $latest = DB::table('product')->orderBy('id','DESC')->limit(3)->get();
        $top = DB::table('product')->orderBy('id')->limit(6)->get();
        //dd($latest);
        return view('index')->with('latest',$latest)->with('top',$top);
    }

    public function show($id)
    {
        $product = Product::findOrFail($id);
        return view('pddetails')->with('product',$product);
    }
    public function all()
    {
        $all = DB::table('product')->paginate(9);
        return view('shop')->with('all',$all);
    }
}
