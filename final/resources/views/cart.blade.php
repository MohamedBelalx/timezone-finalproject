@extends('layouts.main')

@section('content')
  <main>
      <!-- Hero Area Start-->
      <div class="slider-area ">
          <div class="single-slider slider-height2 d-flex align-items-center">
              <div class="container">
                  <div class="row">
                      <div class="col-xl-12">
                          <div class="hero-cap text-center">
                              <h2>Cart List</h2>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <!--================Cart Area =================-->
      <section class="cart_area section_padding">
        <div class="container">
        <a href="{{route('cart.clear')}}" class='btn btn-danger'>Clear</a>
          <div class="cart_inner">
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">Product</th>
                    <th scope="col">Price</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Total</th>
                    <th scope="col">Delete</th>
                  </tr>
                </thead>
                <tbody>
                @foreach(Cart::content() as $product)
                  <tr>
                    <td>
                      <div class="media">
                        <div class="d-flex">
                          <img src="{{asset($product->options->img)}}" alt="" />
                        </div>
                        <div class="media-body">
                          <p>{{$product->name}}</p>
                        </div>
                      </div>
                    </td>
                    <td>
                      <h5>${{$product->price}}</h5>
                    </td>
                    <td>
                      <div class="product_count">
                        <span class="input-number-decrement"> <i class="ti-minus"></i></span>
                        <input class="input-number" type="text" value="{{$product->qty}}" min="0" max="10">
                        <span class="input-number-increment"> <i class="ti-plus"></i></span>
                      </div>
                    </td>
                    <td>
                      <h5>${{$product->price * $product->qty}}</h5>
                    </td>
                    <td><a href="{{route('cart.remove',['id'=>$product->rowId])}}" class='btn btn-danger'>delete</a></td>
                  </tr>
                  @endforeach
                  <tr>
                    <td></td>
                    <td></td>
                    <td>
                      <h5>Subtotal</h5>
                    </td>
                    <td>
                      <h5>${{Cart::subtotal()}}</h5>
                    </td>
                  </tr>
                </tbody>
              </table>
              
              <div class="checkout_btn_inner float-right">
              <form action="{{route('checkout')}}" method="POST">
                  <input type="email" class='mb-3 form-control' name="mail" class="form-control" placeholder='Enter Your Mail'>
                  @csrf
                  <script
                  src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                  data-key="pk_test_vfgfZwHWaFjjcB4YJ36c7isy00CsoHSMt0"
                  data-amount="{{ Cart::subtotal() * 100 }}"
                  data-name="CheckOut From WatchShop"
                  data-description="Buy Watch"
                  data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                  data-locale="auto">
                  </script>
              </form>
              </div>
            </div>
          </div>
      </section>
      <!--================End Cart Area =================-->
  </main>
@endsection