@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">Dashboard</div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <table class="table">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Price</th>
                    <th scope="col">Restore</th>
                    <th scope="col">Delete</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($product as $p)
                    <tr>
                        <th scope="row">{{$p->id}}</th>
                        <td>{{$p->name}}</td>
                        <td>{{$p->price}}</td>
                        <td><a href="{{route('restore',['id' => $p->id])}}">Restore</a></td>
                        <td><a href="{{route('forceDelete',['id' => $p->id])}}">Delete</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
@endsection
