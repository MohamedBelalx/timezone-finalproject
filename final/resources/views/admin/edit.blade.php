@extends('layouts.app')

@section('content')

<div class="card">
        <div class="card-header">Create Product</div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif


            <form action="{{route('update',['id' => $product->id])}}" method='POST' enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="">Product Name : </label>
                    <input type="text" value="{{$product->name}}" name="name" class="form-control" placeholder='Enter Product Name'>
                </div>
                <div class="form-group">
                    <label for="">Product Details : </label>
                    <textarea id="summernote" name="details">{{$product->details}}</textarea>
                </div>
                <div class="form-group">
                    <label for="">Product Price : </label>
                    <input type="number" value="{{$product->price}}" name="price" class="form-control" placeholder='Enter Product Name'>
                </div>

                <div class="form-group">
                    <input type="submit" value="Update" class='btn btn-dark btn-block'>
                </div>


            </form>


        </div>
    </div>


@endsection